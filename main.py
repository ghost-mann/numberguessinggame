import random
import math

#taking inputs
lower = int(input("enter lower bound:- "))

upper = int(input("enter upper bound:- "))

# generating random number between lower and upper

x = random.randint(lower, upper)

print("\n\tYou've only ",
      round(math.log(upper - lower + 1, 2)),
      " chances to guess the integer!\n")

# Initilizing the number of guesses.
count = 0

while count < math.log(upper - lower + 1, 2):
    count += 1

    # taking guessing number as input

    guess = int(input("guess a number:- "))

    # Condition testing
    if x == guess:
        print("Congratulations you did it in ",
              count, " try")
        # Once guessed, loop will break
        break
    elif x > guess:
        print("You guessed too small!")
    elif x < guess:
        print("You Guessed too high!")

# If Guessing is more than required guesses,
# shows this output.
if count >= math.log(upper - lower + 1, 2):
    print("\nThe number is %d" % x)
    print("\tBetter Luck Next time!")